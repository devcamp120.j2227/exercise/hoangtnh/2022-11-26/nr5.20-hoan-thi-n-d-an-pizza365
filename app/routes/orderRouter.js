const express = require("express");
const router = express.Router();

const orderController = require("../controllers/orderController");

router.post("/devcamp-pizza365/orders", orderController.createOrder);
router.get("/devcamp-pizza365/users", orderController.getUser);
router.get("/devcamp-pizza365/orders", orderController.getOrder);
module.exports = router;